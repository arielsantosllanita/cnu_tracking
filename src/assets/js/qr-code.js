var qrcode = new QRCode(document.getElementById("qr-code"), {
  text: "http://localhost:3000/driver/transaction/{{id}}",
  width: 200,
  height: 200,
  colorDark: "#5868bf",
  colorLight: "#ffffff",
  correctLevel: QRCode.CorrectLevel.H,
});
