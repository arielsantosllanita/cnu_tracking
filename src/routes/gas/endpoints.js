'use strict';
/**
 * ## Imports
 *
 */
//Handle the endpoints
var Handlers = require('./handlers'),
  internals = {};

internals.endpoints = [
  {
    method: ["GET"],
    path: "/gas/dashboard",
    handler: Handlers.index,
    config: {
      auth: {
        strategy: "standard",
        scope: ["gas"],
      },
      description: "Dashboard",
      tags: ["api"],
    },
  },
  {
    method: ["GET"],
    path: "/gas/reports",
    handler: Handlers.reports,
    config: {
      auth: {
        strategy: "standard",
        scope: ["gas"],
      },
      description: "Reports",
      tags: ["api"],
    },
  },
  {
    method: ["GET"],
    path: "/gas/settings",
    handler: Handlers.settings,
    config: {
      auth: {
        strategy: "standard",
        scope: ["gas"],
      },
      description: "Settings",
      tags: ["api"],
    },
  },
  {
    method: ["POST"],
    path: "/gas/create-transaction",
    handler: Handlers.create_transaction,
    config: {
      auth: {
        strategy: "standard",
        scope: ["gas"],
      },
      description: "Create transaction",
      tags: ["api"],
    },
  },
];

module.exports = internals;
