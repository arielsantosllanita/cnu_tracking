"use strict";

const { populate } = require("../../database/models/user");

const internals = {},
  Crypto = require("../../lib/Crypto"),
  Cryptos = require("crypto"),
  _ = require("lodash"),
  Async = require("async"),
  moment = require("moment"),
  Users = require("../../database/models/user"),
  Driver = require("../../database/models/driver"),
  GasTransaction = require("../../database/models/gas-transaction");

internals.index = async (req, reply) => {
  try {
    const { _id: officer_in_charge } = req.auth.credentials;
    const today = moment().startOf("day");

    const drivers = await Driver.find({}).lean();
    const transactions = await GasTransaction.find({
      createdAt: {
        $gte: today.toDate(),
        $lt: moment(today).endOf("day").toDate(),
      },
      officer_in_charge,
    })
      .populate("driver routes company")
      .lean();
    // TODO: Find efficient an way to retrive gas cost
    const totalGasCost = transactions.reduce(
      (acc, curr) => (acc += curr.gas_cost),
      0
    );

    reply.view("gas/dashboard.html", {
      message: req.query.message,
      alertType: req.query.alertType,
      success: req.query.success,
      drivers,
      transactions,
      totalGasCost,
      truckTotal: transactions.length,
      user: req.auth.credentials,
    });
  } catch (err) {
    console.error(err);
    reply({ status: 500, message: "Internal error" }).code(500);
  }
};

//REPORTS
internals.reports = async (req, reply) => {
  try {
    const { start_date, end_date, driver } = req.query;
    const query = {
      ...(driver !== "all" && driver != undefined ? { driver } : {}),
      ...(start_date || end_date
        ? {
            updatedAt: {
              ...(start_date ? { $gte: new Date(start_date) } : {}),
              ...(end_date ? { $lt: new Date(end_date) } : {}),
            },
          }
        : {}),
    };

    const drivers = await Driver.find({}).lean();
    const transactions = await GasTransaction.find(query)
      .populate("driver routes company")
      .lean();
    const totalGasCost = transactions.reduce(
      (acc, curr) => (acc += curr.gas_cost),
      0
    );

    reply.view("gas/reports.html", {
      message: req.query.message,
      alertType: req.query.alertType,
      success: req.query.success,
      transactions,
      totalGasCost,
      user: req.auth.credentials,
      truckTotal: transactions.length,
      drivers,
    });
  } catch (err) {
    console.error(err);
    reply({ status: 500, message: "Internal error" }).code(500);
  }
};

//SETTINGS
internals.settings = (req, reply) => {
  reply.view("gas/settings.html", {
    message: req.query.message,
    alertType: req.query.alertType,
    user: req.auth.credentials,
    success: req.query.success,
  });
};

internals.create_transaction = async (req, reply) => {
  try {
    const { _id: officer_in_charge } = req.auth.credentials;
    await GasTransaction.create({ ...req.payload, officer_in_charge });
    reply.redirect(
      "/gas/dashboard?message=Transaction created successfully&alertType=success"
    );
  } catch (err) {
    reply.redirect("/gas/dashboard?message=Internal error!&alertType=error");
  }
};

module.exports = internals;
