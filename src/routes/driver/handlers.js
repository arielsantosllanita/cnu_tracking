"use strict";

const internals = {};
const Transaction = require("../../database/models/transaction");

internals.dashboard = async (req, reply) => {
  try {
    const { id } = req.params;
    const transaction = await Transaction.findById(id)
      .populate("routes company driver officer_in_charge")
      .lean();
    if (!transaction) throw new Error("Invalid transaction ID");

    reply.view("driver/dashboard.html", { transaction, id });
  } catch (err) {
    reply({ status: 500, message: err.message }).code(500);
  }
};

internals.update_transaction = async (req, reply) => {
  try {
    const { status, id } = req.payload;
    await Transaction.updateOne({ _id: id }, { status });

    reply.redirect(`/driver/transaction/${id}`);
  } catch (err) {
    reply({ status: 500, message: "Internal error" }).code(500);
  }
};

module.exports = internals;
