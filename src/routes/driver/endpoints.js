"use strict";

const Handlers = require("./handlers");
const internals = {};

internals.endpoints = [
  {
    method: ["GET"],
    path: "/driver/transaction/{id}",
    handler: Handlers.dashboard,
    config: {
      description: "Dashboard",
      tags: ["api"],
    },
  },
  {
    method: ["POST"],
    path: "/driver/update-transaction",
    handler: Handlers.update_transaction,
    config: {
      description: "Update transaction",
      tags: ["api"],
    },
  },
];

module.exports = internals;
