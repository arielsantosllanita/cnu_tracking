"use strict";
var Handlers = require("./handlers"),
  internals = {};

internals.endpoints = [
  {
    method: ["GET"],
    path: "/staff/dashboard",
    handler: Handlers.index,
    config: {
      auth: {
        strategy: "standard",
        scope: ["staff"],
      },
      description: "Dashboard",
      tags: ["api"],
    },
  },
  {
    method: ["GET"],
    path: "/staff/reports",
    handler: Handlers.reports,
    config: {
      auth: {
        strategy: "standard",
        scope: ["staff"],
      },
      description: "Reports",
      tags: ["api"],
    },
  },
  {
    method: ["GET"],
    path: "/staff/staff",
    handler: Handlers.staff,
    config: {
      auth: {
        strategy: "standard",
        scope: ["staff"],
      },
      description: "Staff",
      tags: ["api"],
    },
  },
  {
    method: ["GET"],
    path: "/staff/settings",
    handler: Handlers.settings,
    config: {
      auth: {
        strategy: "standard",
        scope: ["staff"],
      },
      description: "Settings",
      tags: ["api"],
    },
  },
  {
    method: ["POST"],
    path: "/staff/create-routes",
    handler: Handlers.createRoutes,
    config: {
      auth: {
        strategy: "standard",
        scope: ["staff"],
      },
      description: "Create route",
      tags: ["api"],
    },
  },
  {
    method: ["POST"],
    path: "/staff/create-company",
    handler: Handlers.createCompany,
    config: {
      auth: {
        strategy: "standard",
        scope: ["staff"],
      },
      description: "Create company",
      tags: ["api"],
    },
  },
  {
    method: ["POST"],
    path: "/staff/create-driver",
    handler: Handlers.createDriver,
    config: {
      auth: {
        strategy: "standard",
        scope: ["staff"],
      },
      description: "Create driver",
      tags: ["api"],
    },
  },
  {
    method: ["POST"],
    path: "/staff/create-transaction",
    handler: Handlers.createTransaction,
    config: {
      auth: {
        strategy: "standard",
        scope: ["staff"],
      },
      description: "Create transaction",
      tags: ["api"],
    },
  },
];

module.exports = internals;
