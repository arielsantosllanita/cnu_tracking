"use strict";

const internals = {},
  Crypto = require("../../lib/Crypto"),
  Cryptos = require("crypto"),
  _ = require("lodash"),
  Async = require("async"),
  moment = require("moment"),
  Users = require("../../database/models/user"),
  Route = require("../../database/models/route"),
  Company = require("../../database/models/company"),
  Transaction = require("../../database/models/transaction"),
  Driver = require("../../database/models/driver"),
  GasTransaction = require("../../database/models/gas-transaction");

var colors = require("colors"); // color for console log

function retrieveTotalDelivery(result) {
  return result.transactions.reduce((acc, curr) => (acc += curr.cost), 0);
}

function retrieveDriverIdsInTransaction(result) {
  return result.transactions.map(({ driver }) => driver._id);
}

async function retrieveGasTransaction(driverIds = [], options = {}) {
  return await GasTransaction.find({
    ...options,
    driver: { $in: driverIds },
  })
    .select("gas_cost")
    .lean();
}

function retrieveGasCost(gasTransactionToday) {
  return gasTransactionToday.reduce((acc, curr) => (acc += curr.gas_cost), 0);
}

function retrieveKilometer(transaction) {
  return transaction.reduce((acc, { routes }) => (acc += routes.kilometer), 0);
}

// DASHBOARD
internals.index = async (req, reply) => {
  const today = moment().startOf("day");

  Async.series(
    {
      transactions: (callback) => {
        const query =
          req.query.address != "all" && req.query.address != undefined
            ? { routes: req.query.address }
            : {};
        Transaction.find({
          ...query,
          createdAt: {
            $gte: today.toDate(),
            $lt: moment(today).endOf("day").toDate(),
          },
        })
          .populate("driver routes company")
          .lean()
          .sort({ createdAt: -1 })
          .exec((err, result) =>
            err ? callback(err) : callback(null, result)
          );
      },
      transactionInvoice: (callback) => {
        Transaction.findById(req.query?.transaction)
          .populate("driver routes company")
          .lean()
          .exec((err, result) =>
            err ? callback(err) : callback(null, result)
          );
      },
      routes: (callback) => {
        Route.find({})
          .lean()
          .exec((err, result) =>
            err ? callback(err) : callback(null, result)
          );
      },
      drivers: (callback) => {
        Driver.find({})
          .lean()
          .exec((err, result) =>
            err ? callback(err) : callback(null, result)
          );
      },
      companies: (callback) => {
        Company.find({})
          .lean()
          .exec((err, result) =>
            err ? callback(err) : callback(null, result)
          );
      },
    },
    async function (err, result) {
      try {
        if (err) throw new Error("Internal error");
        const total_delivered = retrieveTotalDelivery(result);
        const driverIds = retrieveDriverIdsInTransaction(result);
        const gasTransactionToday = await retrieveGasTransaction(driverIds, {
          createdAt: {
            $gte: today.toDate(),
            $lt: moment(today).endOf("day").toDate(),
          },
        });
        const gas_cost = retrieveGasCost(gasTransactionToday);

        reply.view("staff/dashboard.html", {
          message: req.query.message,
          alertType: req.query.alertType,
          success: req.query.success,
          transactions: result.transactions || null,
          drivers: result.drivers || null,
          routes: result.routes || null,
          companies: result.companies || null,
          user: req.auth.credentials,
          delivery: result.transactions.length,
          total_delivered,
          gas_cost,
          transaction: req.query.transaction,
          transactionInvoice: result.transactionInvoice,
        });
      } catch (err) {
        reply.redirect(`/staff/dashboard?message=${err.message}&alert=error`);
      }
    }
  );
};

// REPORTS
internals.reports = function (req, reply) {
  const { start_date, end_date, routes, company, driver } = req.query;
  Async.series(
    {
      transactions: (callback) => {
        // Build up query programmatically
        const query = {
          ...(routes !== "all" && routes != undefined ? { routes } : {}),
          ...(company !== "all" && company != undefined ? { company } : {}),
          ...(driver !== "all" && driver != undefined ? { driver } : {}),
          ...(start_date || end_date
            ? {
                createdAt: {
                  ...(start_date ? { $gte: new Date(start_date) } : {}),
                  ...(end_date ? { $lt: new Date(end_date) } : {}),
                },
              }
            : {}),
        };
        Transaction.find(query)
          .lean()
          .sort({ createdAt: -1 })
          .populate("driver routes company")
          .exec((err, result) =>
            err ? callback(err) : callback(null, result)
          );
      },
      drivers: (callback) => {
        Driver.find({})
          .lean()
          .select("firstname lastname")
          .exec((err, result) =>
            err ? callback(err) : callback(null, result)
          );
      },
      companies: (callback) => {
        Company.find({})
          .lean()
          .select("name")
          .exec((err, result) =>
            err ? callback(err) : callback(null, result)
          );
      },
      routes: (callback) => {
        Route.find({})
          .lean()
          .exec((err, result) =>
            err ? callback(err) : callback(null, result)
          );
      },
    },
    async function (err, result) {
      try {
        const total_delivered = retrieveTotalDelivery(result);
        const driverIds = retrieveDriverIdsInTransaction(result);
        const gasTransactionToday = await retrieveGasTransaction(driverIds, {
          ...(start_date || end_date
            ? {
                createdAt: {
                  ...(start_date ? { $gte: new Date(start_date) } : {}),
                  ...(end_date ? { $lt: new Date(end_date) } : {}),
                },
              }
            : {}),
        });
        const gas_cost = retrieveGasCost(gasTransactionToday);
        const total_kilometers = retrieveKilometer(result.transactions);

        reply.view("staff/reports.html", {
          message: req.query.message,
          alertType: req.query.alertType,
          success: req.query.success,
          transactions: result.transactions,
          drivers: result.drivers || null,
          companies: result.companies || null,
          user: req.auth.credentials,
          routes: result.routes || null,
          delivery: result.transactions.length,
          total_delivered,
          gas_cost,
          total_kilometers: total_kilometers.toFixed(2),
        });
      } catch (err) {
        reply({ status: 500, message: "Internal error!" }).code(500);
      }
    }
  );
};

// STAFF
internals.staff = function (req, reply) {
  Driver.find({})
    .lean()
    .exec((err, result) => {
      reply.view("staff/staff.html", {
        message: req.query.message,
        alertType: req.query.alertType,
        success: req.query.success,
        user: req.auth.credentials,
        drivers: result,
      });
    });
};

// SETTINGS
internals.settings = function (req, reply) {
  // Query all routes and companies to DB then display it to frontend
  Async.series(
    {
      routes: (callback) => {
        Route.find({})
          .lean()
          .exec((err, result) =>
            err ? callback(err) : callback(null, result)
          );
      },
      companies: (callback) => {
        Company.find({})
          .lean()
          .exec((err, result) =>
            err ? callback(err) : callback(null, result)
          );
      },
    },
    function (err, results) {
      // TODO: Add action if error occurs
      if (!err) {
        reply.view("staff/settings.html", {
          routes: results.routes || null,
          user: req.auth.credentials,
          companies: results.companies || null,
        });
      }
    }
  );
};

// CREATE ROUTE
internals.createRoutes = function (req, reply) {
  Route.create({ ...req.payload })
    .then(() => reply.redirect("/staff/settings"))
    .catch((err) =>
      reply.redirect("/staff/settings?message=Internal error&alert=error")
    );
};

// CREATE COMPANY
internals.createCompany = function (req, reply) {
  const { name, contact, address } = req.payload;
  Company.create({ name, contact, address })
    .then(() => reply.redirect("/staff/settings"))
    .catch((err) =>
      reply.redirect("/staff/settings?message=Internal error&alert=error")
    );
};

// CREATE DRIVER
internals.createDriver = function (req, reply) {
  Driver.create({ ...req.payload })
    .then(() => reply.redirect("/staff/staff"))
    .catch((err) =>
      reply.redirect("/staff/staff?message=Internal error&alert=error")
    );
};

// CREATE TRANSACTION
internals.createTransaction = async (req, reply) => {
  try {
    const { _id: officer_in_charge } = req.auth.credentials;
    const { id: transaction } = await Transaction.create({
      ...req.payload,
      officer_in_charge,
    });
    reply.redirect(`/staff/dashboard?transaction=${transaction}`);
  } catch (err) {
    reply.redirect("/staff/dashboard?message=Internal error&alert=error");
  }
};

module.exports = internals;
