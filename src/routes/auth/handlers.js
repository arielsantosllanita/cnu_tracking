"use strict";

var internals = {},
  Crypto = require("../../lib/Crypto"),
  Cryptos = require("crypto"),
  _ = require("lodash"),
  Async = require("async"),
  moment = require("moment"),
  Users = require("../../database/models/user");

internals.noscript = function (req, reply) {
  reply.view("noscript.html");
};
internals.error404 = function (req, reply) {
  reply.view("error404.html");
};

internals.web_authentication = function (req, reply) {
  let email = req.payload.email;
  let password = req.payload.password;
  const encryptedPassword = Crypto.encrypt(password);

  Users.findOne({ email: email })
    .lean()
    .exec((err, data) => {
      if (err) {
        return reply.redirect(
          "/login?message=Authentication failed! Error.&alertType=danger"
        );
      }

      if (data != null) {
        if (data.isVoid == false) {
          if (data.password != encryptedPassword) {
            return reply.redirect(
              "/login?message=Authentication failed! Incorrect password.&alertType=danger"
            );
          } else if (data.scope[0] == "staff") {
            // STAFF
            req.cookieAuth.set(data);
            return reply.redirect("/staff/dashboard");
          } else if (data.scope[0] == "gas") {
            // GAS
            req.cookieAuth.set(data);
            return reply.redirect("/gas/dashboard");
          } else if (data.scope[0] == "admin") {
            // ADMIN
            req.cookieAuth.set(data);
            return reply.redirect("/admin/dashboard");
          } else {
            return reply.redirect(
              "/login?message=Invalid Account! Incorrect password.&alertType=danger"
            );
          }
        } else {
          return reply.redirect(
            "/login?message=Authentication failed! No account found deleted.&alertType=danger"
          );
        }
      } else {
        return reply.redirect(
          "/login?message=Authentication failed! No account found.&alertType=danger"
        );
      }
    });
};

module.exports = internals;
