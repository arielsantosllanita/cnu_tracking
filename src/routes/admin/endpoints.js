'use strict';

const Handlers = require("./handlers"),
  internals = {};

internals.endpoints = [
  {
    method: ["GET"],
    path: "/admin/dashboard",
    handler: Handlers.index,
    config: {
      auth: {
        strategy: "standard",
        scope: ["admin"],
      },
      description: "Dashboard",
      tags: ["api"],
    },
  },
  {
    method: ["GET"],
    path: "/admin/reports",
    handler: Handlers.reports,
    config: {
      auth: {
        strategy: "standard",
        scope: ["admin"],
      },
      description: "Reports",
      tags: ["api"],
    },
  },
  {
    method: ["GET"],
    path: "/admin/settings",
    handler: Handlers.settings,
    config: {
      auth: {
        strategy: "standard",
        scope: ["admin"],
      },
      description: "Settings",
      tags: ["api"],
    },
  },
  {
    method: ["POST"],
    path: "/admin/create-user",
    handler: Handlers.create_user,
    config: {
      auth: {
        strategy: "standard",
        scope: ["admin"],
      },
      description: "Settings",
      tags: ["api"],
    },
  },
  {
    method: ["POST"],
    path: "/admin/update-user",
    handler: Handlers.update_user,
    config: {
      auth: {
        strategy: "standard",
        scope: ["admin"],
      },
      description: "Settings",
      tags: ["api"],
    },
  },
];

module.exports = internals;
