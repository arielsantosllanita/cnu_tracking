"use strict";

const GasTransaction = require("../../database/models/gas-transaction");
const Transaction = require("../../database/models/transaction");
const Driver = require("../../database/models/driver");
const User = require("../../database/models/user");
const internals = {},
  Crypto = require("../../lib/Crypto"),
  Cryptos = require("crypto"),
  _ = require("lodash"),
  Async = require("async"),
  moment = require("moment");

function retrieveTotalDelivery(result) {
  return result.reduce((acc, curr) => (acc += curr.cost), 0);
}

function retrieveDriverIdsInTransaction(result) {
  return result.map(({ driver }) => driver._id);
}

async function retrieveGasTransaction(driverIds) {
  return await GasTransaction.find({
    driver: { $in: driverIds },
  })
    .select("gas_cost")
    .lean();
}

function retrieveGasCost(gasTransactionToday) {
  return gasTransactionToday.reduce((acc, curr) => (acc += curr.gas_cost), 0);
}

internals.index = async (req, reply) => {
  try {
    const today = moment().startOf("day");
    const transactions = await Transaction.find({
      createdAt: {
        $gte: today.toDate(),
        $lt: moment(today).endOf("day").toDate(),
      },
    })
      .populate("driver")
      .lean();

    const driverIds = transactions.map(({ driver }) => driver._id);
    const gasTransactionToday = await GasTransaction.find({
      driver: { $in: driverIds },
      createdAt: {
        $gte: today.toDate(),
        $lt: moment(today).endOf("day").toDate(),
      },
    });
    const gas_cost = gasTransactionToday.reduce(
      (acc, curr) => (acc += curr.gas_cost),
      0
    );

    reply.view("admin/dashboard.html", {
      message: req.query.message,
      alertType: req.query.alertType,
      success: req.query.success,
      transactions,
      user: req.auth.credentials,
      gas_cost,
      truck_number: transactions.length,
      total_delivered: retrieveTotalDelivery(transactions),
    });
  } catch (err) {
    reply({ status: 500, message: "Internal error!" }).code(500);
  }
};

// REPORTS
internals.reports = async (req, reply) => {
  try {
    const { start_date, end_date, driver } = req.query;
    const query = {
      ...(driver !== "all" && driver != undefined ? { driver } : {}),
      ...(start_date || end_date
        ? {
            updatedAt: {
              ...(start_date ? { $gte: new Date(start_date) } : {}),
              ...(end_date ? { $lt: new Date(end_date) } : {}),
            },
          }
        : {}),
    };
    const drivers = await Driver.find({}).lean();
    const transactions = await Transaction.find(query)
      .populate("driver")
      .lean();

    const total_delivered = retrieveTotalDelivery(transactions);
    const driverIds = retrieveDriverIdsInTransaction(transactions);
    const gasTransactionToday = await retrieveGasTransaction(driverIds);
    const gas_cost = retrieveGasCost(gasTransactionToday);

    reply.view("admin/reports.html", {
      message: req.query.message,
      alertType: req.query.alertType,
      success: req.query.success,
      user: req.auth.credentials,
      transactions,
      drivers,
      truck_number: transactions.length,
      total_delivered,
      gas_cost,
    });
  } catch (err) {
    console.error(err);
    reply({ status: 500, message: "Internal error" }).code(500);
  }
};

//SETTINGS
internals.settings = async (req, reply) => {
  try {
    let { user: userID } = req.query;
    let userData = null;

    const users = await User.find({
      scope: { $in: [["staff"], ["gas"]] },
    }).lean();

    if (userID) {
      userData = await User.findById(userID).lean();
    }

    reply.view("admin/settings.html", {
      message: req.query.message,
      alert: req.query.alert,
      user: req.auth.credentials,
      success: req.query.success,
      users,
      ...(userID ? { userData } : {}),
    });
  } catch (err) {
    reply({ status: 500, message: "Internal error" }).code(500);
  }
};

// CREATE USER
internals.create_user = async (req, reply) => {
  try {
    await User.create({ ...req.payload, scope: [req.payload.scope] });
    reply.redirect("/admin/settings");
  } catch (err) {
    console.error("ERROR", err);
    reply.redirect("/admin/settings?message=Internal error&alert=error");
  }
};

internals.update_user = async (req, reply) => {
  try {
    const userID = req.payload.id;
    const payload = req.payload;

    // Remove empty values and company id from payload object
    Object.keys(payload).forEach((key) => {
      if (!payload[key] || key == "id") {
        delete payload[key];
      }
    });

    await User.updateOne({ _id: userID }, { ...payload });
    reply.redirect("/admin/settings");
  } catch (err) {
    reply.redirect("/admin/settings?message=Internal error&alert=error");
  }
};

module.exports = internals;
