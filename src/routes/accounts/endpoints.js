'use strict';
/**
 * ## Imports
 *
 */
//Handle the endpoints
var Handlers = require('./handlers'),
  internals = {};

internals.endpoints = [
  {
    method: ['GET'],
    path: '/',
    handler: Handlers.index,
    config: {
      auth: {
        strategy: 'standard',
        // scope: ['']
      },
    },
  },
  {
    method: ['GET'],
    path: '/login',
    handler: Handlers.login,
  },
  {
    method: ['GET'],
    path: '/logout',
    handler: Handlers.logout,
  },
];

module.exports = internals;
