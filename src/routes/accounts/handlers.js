'use strict';

// var internals = {},
//   Crypto = require('../../lib/Crypto'),
//   Cryptos = require('crypto'),
//   _ = require('lodash'),
//   Async = require("async"),
//   moment = require('moment'),

//   Users = require('../../database/models/users');

var internals = {};

internals.index = function (req, reply) {
  reply.view('accounts/login.html', {
    message:req.query.message,
    alertType: req.query.alertType,
    success: req.query.success
  });
};

internals.login = function (req, reply) {
  req.cookieAuth.clear();
  reply.view('accounts/login.html', {
    message:req.query.message,
    alertType: req.query.alertType,
    success: req.query.success
  });
};

internals.logout = function (req, reply) {
  req.cookieAuth.clear();
  return reply.redirect('/login');
};


module.exports = internals;
