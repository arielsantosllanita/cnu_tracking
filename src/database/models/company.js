'use strict';
/**
 * ## Imports
 *
 */
//Mongoose - the ORM
var Mongoose = require('mongoose'),
  ObjectId = Mongoose.Schema.Types.ObjectId,
  Schema = Mongoose.Schema;

const CompanySchema = new Mongoose.Schema(
  {
    name: { type: String, required: true, uppercase: true },
    contact: { type: String, required: true },
    address: { type: String, required: true, uppercase: true },
  },
  {
    timestamps: true,
    _id: true,
  }
);

var Company = Mongoose.model('companies', CompanySchema);

module.exports = Company;
