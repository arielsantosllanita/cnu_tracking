'use strict';

var Mongoose = require('mongoose'),
  ObjectId = Mongoose.Schema.Types.ObjectId,
  Schema = Mongoose.Schema;

const HelperSchema = new Mongoose.Schema({
  firstname: { type: String, required: false },
  lastname: { type: String, required: false },
  middlename: { type: String, required: false },
  address: { type: String, required: false },
  birthday: { type: String, required: false },
  phone_number: { type: String, required: false },
}, {
  timestamps: true, _id: true,
});

var Helper = Mongoose.model('helpers', HelperSchema);

module.exports = Helper;
