"use strict";

const Mongoose = require("mongoose"),
  ObjectId = Mongoose.Schema.Types.ObjectId,
  Schema = Mongoose.Schema;

const GasSchema = new Mongoose.Schema(
  {
    gas_cost: { type: Number, required: false },
    odometer: { type: Number, required: true },
    driver: { type: ObjectId, ref: "drivers", required: true },
    officer_in_charge: { type: ObjectId, ref: "users", required: true },
  },
  {
    timestamps: true,
    _id: true,
  }
);

const GasTransaction = Mongoose.model("gas-transactions", GasSchema);

module.exports = GasTransaction;
