'use strict';
/**
 * ## Imports
 *
 */
//Mongoose - the ORM
var Mongoose = require('mongoose'),
  ObjectId = Mongoose.Schema.Types.ObjectId,
  Schema = Mongoose.Schema;

const StaffSchema = new Mongoose.Schema(
  {
    firstname: { type: String, required: true, uppercase: true },
    lastname: { type: String, required: true, uppercase: true },
    middlename: { type: String, required: false, uppercase: true },
    type: { type: String, required: true, uppercase: true },
    birthday: { type: Date, required: false, uppercase: true },
    phone_number: { type: String, required: true, uppercase: true },
  },
  {
    timestamps: true,
    _id: true,
  }
);

var Staff = Mongoose.model('staffs', StaffSchema);

module.exports = Staff;
