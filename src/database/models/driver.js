'use strict';
/**
 * ## Imports
 *
 */
//Mongoose - the ORM
var Mongoose = require('mongoose'),
  ObjectId = Mongoose.Schema.Types.ObjectId,
  Schema = Mongoose.Schema;

const DriverSchema = new Mongoose.Schema(
  {
    firstname: { type: String, uppercase: true, required: true },
    middlename: { type: String, uppercase: true, required: false },
    lastname: { type: String, uppercase: true, required: true },
    birthday: { type: Date, required: false },
    phone_number: { type: String, required: true },
    helper: { type: String, uppercase: true, required: true },
    plate_number: { type: String, required: true },
    truck: { type: String, required: true },
  },
  {
    timestamps: true,
    _id: true,
  }
);

var Driver = Mongoose.model('drivers', DriverSchema);

module.exports = Driver;
