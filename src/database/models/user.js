'use strict';

var Mongoose = require("mongoose"),
  ObjectId = Mongoose.Schema.Types.ObjectId,
  Schema = Mongoose.Schema,
  Crypto = require("../../lib/Crypto");

const userSchema = new Mongoose.Schema(
  {
    firstname: { type: String, uppercase: true, required: true },
    lastname: { type: String, uppercase: true, required: true },
    middlename: { type: String, uppercase: true, required: false },
    address: { type: String, uppercase: true, required: true },
    birthday: { type: String, required: true },
    phone_number: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    scope: { type: Array, required: true },
    isVoid: { type: Boolean, default: false },
  },
  {
    timestamps: true,
    _id: true,
  }
);

userSchema.pre("save", function (next) {
  this.password = Crypto.encrypt(this.password);
  next();
});

var User = Mongoose.model("users", userSchema);

module.exports = User;
