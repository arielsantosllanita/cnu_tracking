'use strict';

var Mongoose = require("mongoose"),
  ObjectId = Mongoose.Schema.Types.ObjectId,
  Schema = Mongoose.Schema;

const RouteSchema = new Mongoose.Schema(
  {
    address: { type: String, required: true, unique: true },
    kilometer: { type: Number, required: true },
  },
  {
    timestamps: true,
    _id: true,
  }
);

var Route = Mongoose.model('routes', RouteSchema);

module.exports = Route;
