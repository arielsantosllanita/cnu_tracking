'use strict';

var Mongoose = require("mongoose"),
  ObjectId = Mongoose.Schema.Types.ObjectId,
  Schema = Mongoose.Schema;

const TransactionSchema = new Mongoose.Schema(
  {
    routes: { type: ObjectId, ref: "routes", required: true },
    company: { type: ObjectId, ref: "companies", required: true },
    driver: { type: ObjectId, ref: "drivers", required: true },
    officer_in_charge: { type: ObjectId, ref: "users", required: true },
    amount: { type: String, required: true },
    type: { type: String, required: true },
    cost: { type: Number, required: true },
    status: {
      type: String,
      default: "In-transit",
      enum: ["In-transit", "Departed", "Arrived"],
    },
  },
  {
    timestamps: true,
    _id: true,
  }
);

var Transaction = Mongoose.model('transactions', TransactionSchema);

module.exports = Transaction;
