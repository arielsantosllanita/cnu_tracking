'use strict';

var AuthRoutes = require("../routes/auth/endpoints"),
  AccountsRoutes = require("../routes/accounts/endpoints"),
  //STAFF
  StaffRoutes = require("../routes/staff/endpoints"),
  //GAS
  GasRoutes = require("../routes/gas/endpoints"),
  // ADMIN
  AdminRoutes = require("../routes/admin/endpoints"),
  // DRIVER
  DriverRoutes = require("../routes/driver/endpoints"),
  //INTERNALS
  internals = {};

//Concatentate the routes into one array
internals.routes = [].concat(
  AuthRoutes.endpoints,
  AccountsRoutes.endpoints,
  //STAFF
  StaffRoutes.endpoints,
  //GAS
  GasRoutes.endpoints,
  //ADMIN
  AdminRoutes.endpoints,
  DriverRoutes.endpoints
);

//set the routes for the server
internals.init = function (server) {
  server.route(internals.routes);
};

module.exports = internals;
