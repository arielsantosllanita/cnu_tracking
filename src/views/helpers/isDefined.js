/**
 * This helper will check if a value returns a faulty value
 */
const isDefined = (value) => {
  if (value === null) return false;
  if (typeof value !== "number" && value === "") return false;
  if (typeof value === "undefined" || value === undefined) return false;
  if (value !== null && typeof value === "object" && !Object.keys(value).length)
    return false;

  return true;
};

module.exports = isDefined;
